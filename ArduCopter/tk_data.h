#ifndef TK_DATA_H
#define TK_DATA_H 1

struct tk_ardu_data
{
	double x,y,z; // current xyz absolute
	double th,ph; // current angles

	double dest_x,dest_y,dest_z; // desired xyz
	double dest_th,dest_ph; // desired angles
};

tk_ardu_data tk_dat;
#endif
