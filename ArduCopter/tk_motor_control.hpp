#ifndef TK_MOTOR_CONTROL_HPP
#define TK_MOTOR_CONTROL_HPP 1
#include "tk_data.h"
#include "pid.h"
#include <AP_Motors/AP_Motors_Class.h>

/*
	Inside this hpp, create functions that use
    void                set_roll(float roll_in) { _roll_in = roll_in; };        // range -1 ~ +1
    void                set_pitch(float pitch_in) { _pitch_in = pitch_in; };    // range -1 ~ +1
    void                set_yaw(float yaw_in) { _yaw_in = yaw_in; };            // range -1 ~ +1
    void                set_throttle(float throttle_in) { _throttle_in = throttle_in; };   // range 0 ~ 1




*/
void tk_modify_motors(tk_ardu_data tkad, MOTOR_CLASS *mtrs)
{

	// tkad.
	//	theta (-180, 180)
	//	x (-inf, inf)
	//	y (-inf, inf)
	//	throttle (0, 100)


	PID *rollPid 	= 	new PID(0.1, 1.0f, -1.0f, 0.1, 0.01, 0.5); //
	PID *pitchPid 	= 	new PID(0.1, 1.0f, -1.0f, 0.1, 0.01, 0.5); //
	PID *yawPid 	=	new PID(0.1, 1.0f, -1.0f, 0.1, 0.01, 0.5); //
	PID *throttlePid = 	new PID(0.1, 1.0f, 0.0f, 0.1, 0.01, 0.5); //

	float roll 	= mtrs->get_roll();
	float pitch 	= mtrs->get_pitch();
	float yaw 	= mtrs->get_yaw();
	float throttle 	= mtrs->get_throttle();

	// for ease, let's always aim to keep apply yaw only to keep pointed in starting direction
	// apply pitch and roll according to the desired XY motion
	// apply throttle only to keep at designated altitude

	float dth = tkad.dest_th - tkad.th;
	// dth = degrees away from starting orientation

	if (dth < -180)
		dth += 360;
	else if (dth > 180)
		dth -= 360;

	// calculate desired yaw based on dth
	float desired_yaw = dth / 360.0f; // rough calc, scale as needed
	float applied_yaw = yawPid->calculate(desired_yaw, yaw);




	// apply trig to determine desired roll/pitch for PID loop

	float dx = tkad.dest_x - tkad.x;
	float dy = tkad.dest_y - tkad.y;


		// straight linear scale. if dx or dy is greater than TRIM_ZONE, clamp to full pitch and full yaw.
		// otherwise, divide by scale
	float desired_roll  = (dx*dx > TRIM_ZONE*TRIM_ZONE) ? dx/TRIM_SCALE : 1.0f;
	float desired_pitch = (dy*dy > TRIM_ZONE*TRIM_ZONE) ? dy/TRIM_SCALE : 1.0f;


		// adjust how much actual roll and pitch is applied based on how much the orientation differs
	float applied_roll =  cos(dth/57.0f) * rollPid->calculate(desired_roll, roll);
	float applied_pitch = sin(dth/57.0f) * pitchPid->calculate(desired_pitch, pitch);

	mtrs->set_roll(applied_roll);
	mtrs->set_pitch(applied_pitch);
	mtrs->set_yaw(applied_yaw);
	mtrs->set_throttle(0.55f);
}

#endif
