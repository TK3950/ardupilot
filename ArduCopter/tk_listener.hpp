#ifndef TK_LISTENER_HPP
#define TK_LISTENER_HPP 1

#include "tk_data.h"
#include "tk_consts.h"
#include <fstream>

tk_ardu_data tk_getData()
{
	tk_ardu_data tkad; // make a basic structure
	// read from file
	std::ifstream fh;
	fh.open(TK_DATA_FILE_PATH);
	// TODO : check for existence, default any values <<<<<<< HEAD
	// TODO : get this away from using a file, integrate the INS right into a new set of files
	// always read and write in this order
	fh >> tkad.x;
	fh >> tkad.y;
	fh >> tkad.z;

	fh >> tkad.th;
	fh >> tkad.ph;

	fh >> tkad.dest_x;
	fh >> tkad.dest_y;
	fh >> tkad.dest_z;

	fh >> tkad.dest_th;
	fh >> tkad.dest_ph;

	fh.close();
	// always close the file
	return tkad;
}
#endif
